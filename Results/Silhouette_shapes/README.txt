The funciton is going to take the image and all white colored pixels will become transparent. We could also put a background and not have it transparent but this can easily
be done after the image is generated.

The higher resolution the image is the higher resolution the generated art will be but that also takes more time.
Image 0 will always be default, otherwise you can choose which picture to use. Picture 6 is the same as 0 but with a higher resolution.

To add or create another image in here verify that all pixels are truly white as some pictures have white gradients and won't work perfectly. To verify that you could open
the picture in paint and fill the white with a visible colour like red. if all 'white' doesn't get filed that means that the white is not fully white.

The surnames will be randomly fit in the black spaces. The higher the incidence the bigger the name BUT if there is not a big enough space for the name, the name will be smaller.
Be sure to have big enough empty space for big surnames to take those places. Use #1 to see normal surname size.

If you have a good image but the resolution is too small you can find applications that upscale images.