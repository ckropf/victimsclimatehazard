# VictimsClimateHazard

This repository contains the code to generate word clouds based on the surnames of people affected by climate hazard computed with CLIMADA.

## Requirements

Create an environment `victimsart` with CLIMADA and further necessary packages installed

```
mamba env create -f requirements.yml
```

or 

```
conda env create -f requirements.yml
```


