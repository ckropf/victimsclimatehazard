import numpy as np
import pandas as pd
import geopandas as gpd
import math
import time
from pathlib import Path
import os

from climada.entity import ImpactFunc, ImpactFuncSet
from climada.engine import ImpactCalc
from climada.util.api_client import Client
import climada.util.coordinates as u_coord

import glob
import shutil
from deep_translator import GoogleTranslator
import pycountry_convert as pc
import json
import requests

import random
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.colors as mcolors
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
from collections import Counter  
from PIL import Image

import warnings
warnings.filterwarnings("ignore")

from openpyxl import load_workbook
import openpyxl
import xlsxwriter

from surname_functions import translate_df

#class for coloring words in wordcloud
class SimpleGroupedColorFunc(object):
    """Create a color function object which assigns EXACT colors
       to certain words based on the color to words mapping
       Parameters
       ----------
       color_to_words : dict(str -> list(str))
         A dictionary that maps a color to the list of words.
       default_color : str
         Color that will be assigned to a word that's not a member
         of any value from color_to_words.
    """

    def __init__(self, color_to_words, default_color):
        self.word_to_color = {word: color
                              for (color, words) in color_to_words.items()
                              for word in words}

        self.default_color = default_color

    def __call__(self, word, **kwargs):
        return self.word_to_color.get(word, self.default_color)


#this will show the colour legends if the surnames have been split by countries
def show_legends(data):
    categories = data.groupby('Color')['iso_a3'].agg('first').to_dict()
    patches = [mpatches.Patch(color=k, label=v) for k,v in categories.items()]

    plt.legend(handles=patches)
    plt.axis("off")

def generate_art(
    data, hazard_name, silhouette_n=None, split_continent=None, translate=False,
    split_country=None, country_colors=None, continent_colors=None):
    """
    This function creates a WordCloud with the DataFrame 'data' generated before. It has many functionalities:
    - data = DataFrame to read
    - image_n = which image to use (@Results/Images) choose a number from the images inside that folder, more can be added.
    - split_continent = use the same colour for each Surname in the same continent (Asia, Europe, etc.)
    - split_country =  split colours by countries, it needs the have split_continent = True
    - translate = show the translated names into their language of origin (chinese, japanese, etc.)
    """
    timer = time.time()
    
    no_mask=False
    if silhouette_n==0:
        no_mask=True
    
    #default is set to 0
    if silhouette_n is None or not isinstance(silhouette_n, int) or silhouette_n <= 0:
        silhouette_n = 0 
    
    
    if translate is None or translate is False:
        text = dict(zip(data['Surname'].tolist(),data["Affected_people"].tolist()))
    
    elif translate is True and 'Translated Surname' not in data.columns:
        data = translate_df(data)
        text = dict(zip(data['Translated Surname'].tolist(),data["Affected_people"].tolist()))
    
    else:
        text = dict(zip(data['Translated Surname'].tolist(),data["Affected_people"].tolist()))


    image_path = Path(f"Results/Silhouette_shapes/{silhouette_n}.png")
    font_path = Path("Results/Fonts/Sun-ExtA.ttf")
                      
    
    image = Image.open(image_path)
    mask = np.array(image)
    if no_mask==True:
        mask = np.zeros_like(mask)
    
                      
    #to create a higher resolution WordCloud use a higher resolution mask
    #Change 'width','height' and 'max_words' for the wanted image + figsize lower down
    wc = WordCloud(
                    mask=mask,
                    width=4e3,
                    height=4e3,
                    background_color=None,
                    mode="RGBA",
                    font_path = str(font_path),
                    max_words = 1000000
                        ).generate_from_frequencies(text)
    
    
    if split_continent or split_country:
        
        default_color = 'black'

        if split_country:
            colors = country_colors
            data['Color'] = data['iso_a3'].map(colors)
        else:
            colors = continent_colors
            data['Color'] = data['Continent'].map(colors)

            
        #initialize colors for each colour
        if 'Translated Surname' in data.columns:
            color_to_words = data.groupby('Color')['Translated Surname'].agg(list).to_dict()
        else:
            color_to_words = data.groupby('Color')['Surname'].agg(list).to_dict()


            
        # Create a color function with single tone
        grouped_color_func = SimpleGroupedColorFunc(color_to_words, default_color)
        wc.recolor(color_func=grouped_color_func)


    plt.figure(figsize=(25, 25))
    plt.imshow(wc, interpolation="bilinear")
    plt.axis('off')
    plt.show()
    
    
    
    #print all plots in the same folder
    print('saving plot')
    save_folder = Path("Results/Plots")
    
    counter = 1
    save_path = os.path.join(save_folder, f"{counter}.png")
    
    while os.path.exists(save_path):
        counter += 1
        save_path = os.path.join(save_folder, f"{counter}.png")
    
    wc.to_file(save_path)
    
    
    print(f"timer: {np.round(time.time() - timer,2)}.")
    return data