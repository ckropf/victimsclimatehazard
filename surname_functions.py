import numpy as np
import pandas as pd
import geopandas as gpd
import math
import time
from pathlib import Path
import os


from climada.entity import ImpactFunc, ImpactFuncSet
from climada.engine import ImpactCalc
from climada.util.api_client import Client
import climada.util.coordinates as u_coord


import glob
import shutil
from deep_translator import GoogleTranslator
import pycountry_convert as pc
import json
import requests

import random
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.colors as mcolors
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
from collections import Counter
from PIL import Image

import warnings
warnings.filterwarnings("ignore")

from openpyxl import load_workbook
import openpyxl
import xlsxwriter


#generate another result of the world but by combining all other results from all other countries
def sum_world(hazard_name):
    folder_path = Path(f"Impacts/{hazard_name}")
    csv_files = glob.glob(f"{folder_path}/*.txt")
    concat = pd.DataFrame()

    for file in csv_files:
        if file.endswith('world.txt'):
            continue
        data = pd.read_csv(file, sep='\t')
        concat = pd.concat([concat, data])

    pivot_table = pd.pivot_table(concat, index=['Surname'], values=['Incidence', "Affected_people", 'Frequency', 'Total_affected_people'], aggfunc='sum')
    pivot_table = pivot_table.sort_values(by="Affected_people", ascending=False)
    pivot_table['Frequency'] = None
    pivot_table.insert(0, 'Rank', range(1, len(pivot_table)+1))
    pivot_table.insert(1, 'Surname', pivot_table.index)
    pivot_table.insert(4, 'WORLD', '')
    pivot_table['WORLD'][0] = 7287410582

    total_sum = pivot_table['Total_affected_people'].sum()  #total people that have been affected
    del pivot_table['Total_affected_people']
    pivot_table['Total_affected_people'] = None
    pivot_table['Total_affected_people'][0] = total_sum

    output_path = Path(f"Impacts/All_affected_people_worldwide_{hazard_name}.txt")
    pivot_table.to_csv(output_path,sep='\t',index=False)
    return pivot_table


#This creates a DataFrame with the top {number} Surnames in the world and gives their iso_3a code
#The DataFrame will be used afterwards to plot the results

def country_origin_names(n_names, hazard_name):
    # Check if the number is positive and greater than zero
    if n_names <= 0:
        print(f"The number: {n_names} must be a positive number and greater than 0")
        return

    # File path to the "world.xlsx" file ; copy all names in the world
    folder_path = Path(f"Impacts/All_affected_people_worldwide_{hazard_name}.txt")

    # Read the contents of "world." into a DataFrame called 'mother'
    mother = pd.read_csv(folder_path,sep='\t')

    #remove all rows after the desired number
    mother = mother.iloc[:n_names]


    # Insert a new column named "Origin" at index 2, initialized with None values
    mother.insert(2,'Origin', None)
    mother.insert(3,'iso_a3',None)

    #open the done Excel and copy the already calculated Origins
    done_path = Path(f"Impacts/Top_{n_names}_affected_people_worldwide_{hazard_name}.txt")

    new = False

    if done_path.exists():
        done = pd.read_csv(done_path,sep='\t')
    else:
        #create empty done DataFrame for future uses
        mother.to_csv(done_path,sep='\t',index=False)
        done = mother
        new = True


    #copy all already calculated data from 'done' to mother
    for i in range(len(mother['Surname'])):
        surname = mother['Surname'][i]
        if surname in done['Surname'].values:
            origin = done[done['Surname'] == surname]['Origin'].values[0]
            mother.loc[mother['Surname'] == surname, 'Origin'] = origin

            iso_code = done[done['Surname'] == surname]['iso_a3'].values[0]
            mother.loc[mother['Surname']== surname, 'iso_a3'] = iso_code

    #look at all the CSV files of countries
    folder_path = Path(f"Impacts/{hazard_name}")
    CSV_files = glob.glob(f"{folder_path}/*.txt")  # Get a list of CSV files in the specified directory

    # Iterate over the surnames in the 'Surname' column of an unspecified DataFrame called 'data'
    for name, origin in zip(mother['Surname'], mother['Origin']):
        # Check if the 'Origin' column is already filled for the current surname
        if not pd.isnull(origin):
            continue  # Skip to the next iteration if 'Origin' is already filled

        highest_incidence = 0  # Initialize the highest incidence to 0
        country_name = None  # Initialize the country name to None
        iso_a3 = None


        # Loop over the files in 'excel_files'
        for file in CSV_files:
            # Skip the file if its name ends with 'world.xlsx'
            if file.endswith('world.txt'):
                continue

            # Read the contents of the current file into a DataFrame called 'sister'
            sister = pd.read_csv(file,sep='\t')

            # Check if the current 'name' exists in the 'Surname' column of 'sister'
            if name in sister['Surname'].values:
                # Select the rows in 'sister' where 'Surname' matches the current 'name'
                name_sister = sister[sister['Surname'] == name]

            # Check if the maximum value in the 'XXX_impcts' column of 'name_sister' is greater than the current 'highest_incidence'
                if (name_sister[f"Affected_people"] > highest_incidence).any():
                # Update 'highest_incidence' with the new maximum value
                    highest_incidence = name_sister[f"Affected_people"].max()

                # Assign the country name from the first row of 'sister' DataFrame to 'country_name'
                    country_name = sister.iloc[0, 6]
                    iso_a3 = sister.columns[6]

        # Update the 'Origin' column of the 'data' DataFrame where 'Surname' matches the current 'name' with 'country_name'
        mother.loc[mother['Surname'] == name, 'Origin'] = country_name
        mother.loc[mother['Surname'] == name, 'iso_a3'] = iso_a3

    mother.to_csv(done_path, sep='\t',index=False)
    print('Excel file saved')

    # Return the result of calling a function named 'style_surnames' with the modified 'data' DataFrame as an argument
    return mother

#create a DataFrame with each iso_a3 code, the total number of impacted people and a % of the impacted people
#this is used to create WordClouds but by limiting a % of the total impacted population of the country
def get_impcts_by_country(percent, hazard_name):

    folder_path = Path(f"Impacts/{hazard_name}")
    CSV_files = glob.glob(f"{folder_path}/*.txt")  # Get a list of Excel files in the specified directory

    values = []
    Name   = []

    for file in CSV_files:
        data = pd.read_csv(file,sep='\t')
        if file.endswith('world.txt'):
            continue
        else:
            impact = data.iloc[0,8]  #get the impacts, check whether this is truly the right column
        if impact <100:
            continue

        values.append(int(impact))

        file_name = os.path.splitext(os.path.basename(file))[0]
        Name.append(file_name)

    data = {'iso_a3' : Name, 'impacted' : values}
    df = pd.DataFrame(data)
    df = df.sort_values('impacted',ascending=False)
    df = df.reset_index(drop=True)

    df['limited'] = df['impacted']*percent/100
    return df



#create a new dataframe by limiting the number of Surnames for each country according to percent
#percent = 10 --> the sum of the impacted people of all surnames won't go higher than 10
def limited_df(df, percent, hazard_name):
    data1 = df.copy()
    data2 = get_impcts_by_country(percent, hazard_name)
    final_df = pd.DataFrame(columns=data1.columns)
    blacklist = []
    for code in data1['iso_a3']:
        if code in blacklist:
            continue
        #add the iso_a3 to blacklist
        blacklist.append(code)
        #copy a block with all surnames with the same iso_a3 code
        block = df[df['iso_a3'] == code]
        #print(block)

        #get the limited information from data2
        limited_row = data2[data2['iso_a3'] == code]
        limited = limited_row['limited'].values[0]

        #print(f"limited = {limited}, for {code}")
        #initialize sum appended
        sum_impcts = 0
        for i in range(len(block)):
            row_block = pd.DataFrame(block.iloc[i]).transpose()
            added = row_block["Affected_people"].values[0]
            #print(f"added = {added} for {row_block}")
            if ((added + sum_impcts) < limited).any():
                sum_impcts+=added
                #print(sum_impcts)
                final_df = final_df.append(row_block,ignore_index=True)
            elif (sum_impcts >= limited).any():
                print(f"for {code} , we arrived at {sum_impcts/limited *100} of the limit")
                break

    return final_df


#this translates the entire DataFrame, it copies the already calculated translation from 'Translated.txt'
#and calculates the rest
#it also calculates the continent
def translate_df(data, translator=GoogleTranslator):
    path = Path(f"Results/translate/Top_{data.shape[0]}_affected_people_worldwide_translated.txt")
    translated_df = data.copy()
    translated_df['Translated Surname'] = None
    translated_df['Continent'] = None

    for index, row in data.iterrows():
        data.loc[index,'Translated Surname'] = translate_surnames(row, translator)
        iso = row['iso_a3']
        data.loc[index,'Continent'] = get_continent(iso)

    translated_df.to_csv(path,sep='\t',index=False)
    print(f"New data was saved")

    return data

#get json dictionary with all iso_a3 codes : code to translate
def get_translation():
    path = Path('Results/translate/languages.json')
    with open(path, 'r') as file:
        data = json.load(file)
    return data


#translate Surnames with the corresponding language found in the dictionary
def translate_surnames(row, translator):
    languages = get_translation()  # dictionary with all iso_a3 codes and the corresponding code to translate
    iso_a3 = row['iso_a3'].upper()
    surname = row['Surname']
    if iso_a3 in list(languages.keys()):
        try:
            translation = translator(src='en', dest=languages[iso_a3]).translate(surname)
            return translation
        except Exception as e:
            print('Translation error for :', surname,'   ', e)
            return surname
    else:
        print(f"Language not found for {iso_a3}")
        return surname


#Return the continent the country iso_a3 code is from (Asia, Africa, Europe,etc.)
def get_continent(country_code):
    try:
        country_alpha2 = pc.country_alpha3_to_country_alpha2(country_code)
        continent_code = pc.country_alpha2_to_continent_code(country_alpha2)
        continent = pc.convert_continent_code_to_continent_name(continent_code)
        return continent
    except:
        print('Error')
        return None

