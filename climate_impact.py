import numpy as np
import pandas as pd
import geopandas as gpd
import math
import time
from pathlib import Path
import os


from climada.entity import ImpactFunc, ImpactFuncSet
from climada.engine import ImpactCalc
from climada.util.api_client import Client
import climada.util.coordinates as u_coord


import glob
import shutil
from deep_translator import GoogleTranslator
import pycountry_convert as pc
import json
import requests

import random
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.colors as mcolors
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
from collections import Counter  
from PIL import Image

import warnings
warnings.filterwarnings("ignore")

from openpyxl import load_workbook
import openpyxl
import xlsxwriter


#initialize dataframe with country names and iso3alpha codes (remove iso-codes that we don't have the data for)
def country_df():
    world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
    # Extract the relevant columns (country name, iso_a3)
    data = world[['name','iso_a3']]
    iso_removes = ['-99', 'CYN','SOL','EST', 'ESH'] #remove countries that pose problems (no data, no representation, etc.)
    #kosovo, northen cypros, somaliland, ? , western sahara
    data = data[~data['iso_a3'].isin(iso_removes)].reset_index(drop=True)
    return data

def generate_hazard(country_iso, hazard_name, properties):
    properties['country_iso3alpha'] = country_iso
    client = Client()
    return client.get_hazard(hazard_name, properties = properties)

def generate_impfset(haz_type, threshold_intensity):
    impf = ImpactFunc.from_step_impf(haz_type=haz_type, intensity = (0, threshold_intensity, 10*threshold_intensity), impf_id=1)
    return ImpactFuncSet([impf])

def generate_exposures(country_iso):
    client = Client()
    return client.get_litpop(country=country_iso, exponents=(0,1))

def generate_impacts(country_df, threshold_intensity, hazard_name, hazard_properties):

    #input and output path for reading the Excel we've made with all the info of the countries and creating a copy and adding
    #the people affected by the various hazards 
    input_path = Path("Data/World_population.xlsx")
        
    for cnt_iso in country_df['iso_a3']:
    
        output_path = Path(f"Impacts/{hazard_name}/{cnt_iso}.txt")
        output_path.parent.mkdir(parents=True, exist_ok=True)

        if output_path.exists():
            print(f"The data file for {cnt_iso} already exists. Skiping to next country")
            continue
     
        haz = generate_hazard(cnt_iso, hazard_name, hazard_properties)
        impfset = generate_impfset(haz.haz_type, threshold_intensity)
        exp = generate_exposures(cnt_iso)
        exp.gdf[f'impf_{haz.haz_type}'] =  1
    
        impact = ImpactCalc(exp, impfset, haz).impact()
    
        #give the total number of affected people in country to results
        result = int(impact.aai_agg)

        df = pd.read_excel(input_path, sheet_name=cnt_iso) 
    
        df['Affected_people'] = (df.iloc[:,3] * result).astype(float).fillna(0).astype(int) #the sum of people affected, multiplied by frequency
        df['Total_affected_people'] = [(result)] + [None] *(len(df)-1) #simply show the total sum

        df.to_csv(output_path, sep='\t',index=False)   

        print(f"Country {cnt_iso} done")
        