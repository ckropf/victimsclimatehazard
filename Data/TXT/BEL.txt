1	Peeters	41,260	1:279
2	Janssens	37,577	1:306
3	Maes	31,907	1:360
4	Jacobs	25,300	1:454
5	Mertens	23,591	1:487
6	Willems	23,325	1:493
7	Claes	20,551	1:559
8	Wouters	19,874	1:578
9	Goossens	19,552	1:588
10	Dubois	18,272	1:629
11	de Smet	17,411	1:660
12	Pauwels	16,685	1:689
13	Vermeulen	16,681	1:689
14	Hermans	16,474	1:698
15	Lambert	16,304	1:705
16	Aerts	15,584	1:738
17	Michiels	14,989	1:767
18	Dupont	14,956	1:769
19	Martens	13,752	1:836
20	de Vos	13,617	1:844
21	Smets	13,292	1:865
22	Martin	13,056	1:881
23	van de Velde	13,048	1:881
24	Desmet	12,963	1:887
25	Hendrickx	12,787	1:899
26	van Damme	12,718	1:904
27	Claeys	12,711	1:904
28	Devos	12,519	1:918
29	Janssen	12,407	1:927
30	Stevens	12,295	1:935
31	Dumont	11,848	1:970
32	de Clercq	11,742	1:979
33	de Backer	11,549	1:995
34	Segers	11,432	1:1,006
35	Lemmens	11,198	1:1,027
36	Leroy	11,065	1:1,039
37	van den Broeck	10,986	1:1,046
38	Coppens	10,801	1:1,064
39	Simon	10,393	1:1,106
40	Wauters	10,108	1:1,137
41	Laurent	10,094	1:1,139
42	Leclercq	10,066	1:1,142
43	Renard	9,745	1:1,180
44	Denis	9,549	1:1,204
45	Verhoeven	9,356	1:1,229
46	Lejeune	9,311	1:1,235
47	Cools	8,980	1:1,280
48	Declercq	8,920	1:1,289
49	de Cock	8,748	1:1,314
50	Petit	8,726	1:1,318
51	Charlier	8,725	1:1,318
52	Thomas	8,701	1:1,321
53	Lemaire	8,647	1:1,330
54	Smet	8,522	1:1,349
55	Timmermans	8,488	1:1,354
56	de Smedt	8,397	1:1,369
57	de Meyer	8,374	1:1,373
58	Michel	8,303	1:1,385
59	Vandenberghe	8,284	1:1,388
60	Thys	8,154	1:1,410
61	Bertrand	8,115	1:1,417
62	de Ridder	8,106	1:1,418
63	Lenaerts	8,072	1:1,424
64	Cornelis	8,037	1:1,430
65	de Wilde	8,021	1:1,433
66	Bauwens	7,993	1:1,438
67	de Pauw	7,990	1:1,439
68	Verheyen	7,973	1:1,442
69	van den Bossche	7,901	1:1,455
70	Lefebvre	7,897	1:1,456
71	Mathieu	7,863	1:1,462
72	Carlier	7,854	1:1,464
73	Evrard	7,826	1:1,469
74	Baert	7,759	1:1,482
75	Verstraete	7,750	1:1,483
76	Moens	7,698	1:1,493
77	Lauwers	7,661	1:1,501
78	Bernard	7,633	1:1,506
79	Lambrechts	7,616	1:1,510
80	Bosmans	7,434	1:1,546
81	Bogaert	7,405	1:1,553
82	Geerts	7,388	1:1,556
83	Fontaine	7,375	1:1,559
84	Bogaerts	7,264	1:1,583
85	Verlinden	7,262	1:1,583
86	Gerard	7,243	1:1,587
87	Christiaens	7,096	1:1,620
88	Deprez	7,071	1:1,626
89	Verbeke	7,015	1:1,639
90	Vermeiren	7,009	1:1,640
91	Verschueren	6,920	1:1,661
92	Marchal	6,918	1:1,662
93	Beckers	6,886	1:1,670
94	Delvaux	6,885	1:1,670
95	Legrand	6,884	1:1,670
96	Jansen	6,860	1:1,676
97	Wuyts	6,760	1:1,701
98	Adam	6,751	1:1,703
99	Simons	6,664	1:1,725
100	Verstraeten	6,605	1:1,741
101	Collard	6,562	1:1,752
102	Moreau	6,559	1:1,753
103	Verhaeghe	6,522	1:1,763
104	Claessens	6,484	1:1,773
105	Mahieu	6,454	1:1,781
106	Vermeersch	6,453	1:1,782
107	Vandamme	6,441	1:1,785
108	Thijs	6,374	1:1,804
109	Nijs	6,356	1:1,809
110	Henry	6,328	1:1,817
111	Robert	6,285	1:1,829
112	Lefevre	6,269	1:1,834
113	Jacques	6,214	1:1,850
114	Pieters	6,205	1:1,853
115	Thiry	6,195	1:1,856
116	Heylen	6,158	1:1,867
117	van Hoof	6,151	1:1,869
118	de Groote	6,123	1:1,878
119	D'Hondt	6,102	1:1,884
120	Verhaegen	6,074	1:1,893
121	Ceulemans	6,060	1:1,897
122	van Dyck	6,043	1:1,902
123	Goethals	6,042	1:1,903
124	Lievens	5,990	1:1,919
125	François	5,981	1:1,922
126	Vandevelde	5,975	1:1,924
127	Callens	5,964	1:1,928
128	de Coster	5,958	1:1,930
129	Noel	5,925	1:1,940
130	van Hove	5,909	1:1,946
131	van Hecke	5,885	1:1,954
132	Servais	5,854	1:1,964
133	Raes	5,842	1:1,968
134	Bastin	5,822	1:1,975
135	de Bruyn	5,807	1:1,980
136	Leemans	5,792	1:1,985
137	de Coninck	5,745	1:2,001
138	Verbruggen	5,730	1:2,006
139	Parmentier	5,687	1:2,022
140	Dierckx	5,672	1:2,027
141	Vercruysse	5,620	1:2,046
142	Gillet	5,613	1:2,048
143	Verhelst	5,602	1:2,052
144	van Acker	5,594	1:2,055
145	Roels	5,570	1:2,064
145	Vercammen	5,570	1:2,064
147	Hubert	5,553	1:2,070
148	Lebrun	5,544	1:2,074
149	Gielen	5,521	1:2,082
150	Luyten	5,514	1:2,085
150	Somers	5,514	1:2,085
152	Rousseau	5,499	1:2,091
153	Guillaume	5,493	1:2,093
154	Cuypers	5,457	1:2,107
155	Descamps	5,453	1:2,108
156	Daems	5,428	1:2,118
157	Matthys	5,394	1:2,131
158	de Decker	5,390	1:2,133
159	Toussaint	5,361	1:2,144
160	Vandewalle	5,357	1:2,146
161	Mortier	5,349	1:2,149
162	Goffin	5,345	1:2,151
163	Vercauteren	5,333	1:2,156
164	Verdonck	5,332	1:2,156
165	Herman	5,323	1:2,160
165	Swinnen	5,323	1:2,160
167	van den Bergh	5,315	1:2,163
168	Smeets	5,286	1:2,175
169	Andries	5,258	1:2,187
170	de Bruyne	5,239	1:2,194
171	Louis	5,238	1:2,195
172	Collin	5,233	1:2,197
173	van den Berghe	5,230	1:2,198
174	Huybrechts	5,199	1:2,211
175	Vervoort	5,180	1:2,219
176	Peters	5,174	1:2,222
177	de Witte	5,170	1:2,224
178	Michaux	5,135	1:2,239
179	Nys	5,112	1:2,249
180	Verbeeck	5,105	1:2,252
181	de Winter	5,078	1:2,264
182	de Keyser	5,071	1:2,267
183	Meert	5,061	1:2,272
184	de Bock	5,055	1:2,274
185	Vanneste	5,053	1:2,275
186	Declerck	5,042	1:2,280
187	Vos	5,034	1:2,284
188	Antoine	4,995	1:2,302
189	van Den Eynde	4,976	1:2,310
190	Cornet	4,972	1:2,312
191	Dierickx	4,940	1:2,327
192	de Boeck	4,928	1:2,333
193	Verhulst	4,926	1:2,334
194	Claus	4,911	1:2,341
195	Verheyden	4,907	1:2,343
196	Dewulf	4,904	1:2,344
197	van Daele	4,846	1:2,372
198	Coenen	4,834	1:2,378
199	de Wolf	4,833	1:2,379
200	Moons	4,820	1:2,385
201	Huysmans	4,819	1:2,386
202	Jans	4,809	1:2,391
203	Geens	4,789	1:2,401
204	Deckers	4,762	1:2,414
205	Poncelet	4,750	1:2,420
205	Remy	4,750	1:2,420
207	Smits	4,724	1:2,434
208	Vanhove	4,706	1:2,443
209	Baeyens	4,696	1:2,448
209	Lecomte	4,696	1:2,448
211	de Clerck	4,683	1:2,455
211	de Waele	4,683	1:2,455
213	Reynders	4,682	1:2,455
214	de Wit	4,653	1:2,471
215	Engelen	4,647	1:2,474
216	van den Bosch	4,587	1:2,506
217	Dhondt	4,584	1:2,508
218	Meeus	4,576	1:2,512
219	Libert	4,572	1:2,515
220	Vandenbussche	4,541	1:2,532
221	van Goethem	4,528	1:2,539
222	Decoster	4,523	1:2,542
223	Cuvelier	4,469	1:2,573
224	Lacroix	4,468	1:2,573
225	Vandeputte	4,465	1:2,575
226	Vanhoutte	4,460	1:2,578
227	Engels	4,450	1:2,584
228	David	4,445	1:2,586
229	Massart	4,434	1:2,593
230	Luyckx	4,394	1:2,616
231	Vandendriessche	4,379	1:2,625
232	Gilson	4,374	1:2,628
233	Dujardin	4,373	1:2,629
234	Heymans	4,359	1:2,637
235	de Greef	4,355	1:2,640
236	Delhaye	4,352	1:2,642
237	Schepers	4,349	1:2,644
238	Schmitz	4,329	1:2,656
239	Houben	4,326	1:2,658
240	van Camp	4,325	1:2,658
241	Sterckx	4,321	1:2,661
242	Meunier	4,312	1:2,666
243	Francois	4,292	1:2,679
244	Joris	4,280	1:2,686
245	Garcia	4,274	1:2,690
246	Maertens	4,272	1:2,691
247	Eeckhout	4,268	1:2,694
247	Goris	4,268	1:2,694
249	Vaes	4,259	1:2,699
250	van de Walle	4,218	1:2,726
251	Temmerman	4,215	1:2,728
252	Decock	4,213	1:2,729
253	Bekaert	4,183	1:2,748
254	Vandenbroucke	4,178	1:2,752
255	van den Brande	4,162	1:2,762
256	Vermeire	4,140	1:2,777
257	de Moor	4,113	1:2,795
258	de Schepper	4,103	1:2,802
259	Vincent	4,100	1:2,804
260	Huyghe	4,092	1:2,810
261	Leonard	4,083	1:2,816
262	Piette	4,065	1:2,828
263	Dewaele	4,047	1:2,841
264	de Paepe	4,041	1:2,845
265	van de Voorde	4,034	1:2,850
266	Bollen	4,033	1:2,851
267	Debruyne	4,032	1:2,851
268	Bodart	4,024	1:2,857
269	Desmedt	4,021	1:2,859
270	Roland	4,010	1:2,867
271	Boulanger	3,981	1:2,888
272	Bourgeois	3,972	1:2,894
273	Gérard	3,970	1:2,896
273	Lambrecht	3,970	1:2,896
275	Georges	3,969	1:2,897
276	Piron	3,960	1:2,903
277	Moerman	3,953	1:2,908
278	van Belle	3,948	1:2,912
279	Gilles	3,926	1:2,928
279	Ooms	3,926	1:2,928
281	de Jonghe	3,919	1:2,934
282	Hardy	3,911	1:2,940
283	Nuyts	3,876	1:2,966
284	van Roy	3,873	1:2,968
285	Bracke	3,871	1:2,970
286	Pierre	3,870	1:2,971
287	Baeten	3,867	1:2,973
288	André	3,858	1:2,980
289	Merckx	3,855	1:2,982
290	Vandaele	3,826	1:3,005
291	van Herck	3,819	1:3,010
292	Blomme	3,818	1:3,011
293	Delfosse	3,814	1:3,014
294	Boonen	3,808	1:3,019
295	Everaert	3,784	1:3,038
296	Brasseur	3,783	1:3,039
297	Schepens	3,782	1:3,040
298	Vranken	3,779	1:3,042
299	van Looy	3,778	1:3,043
300	Etienne	3,760	1:3,058
301	Marien	3,756	1:3,061
302	Torfs	3,751	1:3,065
303	Vanderstraeten	3,738	1:3,076
304	Boone	3,732	1:3,081
305	Vrancken	3,703	1:3,105
306	Dethier	3,691	1:3,115
307	Borremans	3,680	1:3,124
308	Lecocq	3,678	1:3,126
309	Vanderlinden	3,677	1:3,127
310	Callewaert	3,669	1:3,133
311	van Driessche	3,666	1:3,136
312	Collignon	3,665	1:3,137
312	Daniels	3,665	1:3,137
314	Dufour	3,664	1:3,138
315	van Dijck	3,656	1:3,145
316	de Laet	3,650	1:3,150
317	Jacob	3,642	1:3,157
318	Verbist	3,638	1:3,160
319	Tanghe	3,597	1:3,196
320	Helsen	3,589	1:3,203
321	Driesen	3,579	1:3,212
322	Govaerts	3,572	1:3,219
323	Verelst	3,570	1:3,220
324	Diallo	3,568	1:3,222
325	van Assche	3,552	1:3,237
326	Bonte	3,548	1:3,240
326	Marechal	3,548	1:3,240
328	de Bie	3,543	1:3,245
329	Pirotte	3,529	1:3,258
330	de Block	3,524	1:3,262
331	Fernandez	3,495	1:3,289
332	Poppe	3,470	1:3,313
333	Nicolas	3,465	1:3,318
334	Colson	3,460	1:3,323
335	Masson	3,458	1:3,325
336	Pirard	3,454	1:3,329
337	Hoste	3,444	1:3,338
338	de Roeck	3,432	1:3,350
338	Theys	3,432	1:3,350
340	Leys	3,411	1:3,370
341	Wellens	3,388	1:3,393
342	Vanderheyden	3,380	1:3,401
343	Pollet	3,372	1:3,409
344	Buelens	3,364	1:3,418
345	Stas	3,363	1:3,419
346	Jonckheere	3,360	1:3,422
346	Leysen	3,360	1:3,422
348	Baetens	3,353	1:3,429
349	Bodson	3,346	1:3,436
349	Carpentier	3,346	1:3,436
351	de Maeyer	3,328	1:3,455
352	van Loo	3,314	1:3,469
353	Geeraerts	3,313	1:3,470
354	van Laere	3,310	1:3,473
355	van Rompaey	3,294	1:3,490
356	Renders	3,283	1:3,502
357	Vandekerckhove	3,277	1:3,508
358	Mercier	3,274	1:3,511
359	Moors	3,255	1:3,532
360	op de Beeck	3,254	1:3,533
361	Tack	3,251	1:3,536
362	Pattyn	3,249	1:3,539
363	Gregoire	3,243	1:3,545
364	Cambier	3,236	1:3,553
365	Urbain	3,233	1:3,556
366	Naessens	3,220	1:3,570
367	Chevalier	3,209	1:3,583
368	Vandecasteele	3,191	1:3,603
369	van Dessel	3,189	1:3,605
370	Philips	3,181	1:3,614
370	Piret	3,181	1:3,614
372	Allard	3,174	1:3,622
372	Jacquet	3,174	1:3,622
374	Olivier	3,151	1:3,649
375	Callebaut	3,143	1:3,658
375	Marchand	3,143	1:3,658
377	Delcourt	3,134	1:3,668
378	Deconinck	3,130	1:3,673
379	Dobbelaere	3,128	1:3,675
380	Gillard	3,124	1:3,680
381	Muller	3,113	1:3,693
382	Nguyen	3,100	1:3,709
383	Pirson	3,099	1:3,710
384	Mestdagh	3,089	1:3,722
385	Seghers	3,083	1:3,729
386	Viaene	3,079	1:3,734
387	Lippens	3,074	1:3,740
388	Thirion	3,061	1:3,756
389	de Baets	3,056	1:3,762
389	de Neve	3,056	1:3,762
391	Jacquemin	3,045	1:3,776
392	de Vuyst	3,043	1:3,778
393	Berger	3,035	1:3,788
394	Denys	3,021	1:3,806
395	Roose	3,018	1:3,809
396	Gobert	3,015	1:3,813
397	Goovaerts	3,011	1:3,818
397	Roelandt	3,011	1:3,818
399	Melis	3,009	1:3,821
400	Verboven	2,997	1:3,836
401	Vansteenkiste	2,995	1:3,839
402	Cordier	2,994	1:3,840
403	Arnould	2,989	1:3,846
404	Staelens	2,985	1:3,851
405	van Gorp	2,984	1:3,853
406	Cox	2,982	1:3,855
407	de Sutter	2,972	1:3,868
408	Hanssens	2,966	1:3,876
408	Stroobants	2,966	1:3,876
410	Heyvaert	2,960	1:3,884
411	Colin	2,958	1:3,887
412	Gillis	2,954	1:3,892
413	Demoulin	2,953	1:3,893
414	Dewitte	2,946	1:3,902
414	Smolders	2,946	1:3,902
416	Bruyninckx	2,933	1:3,920
417	Thibaut	2,929	1:3,925
418	de Schrijver	2,928	1:3,926
419	Depoorter	2,923	1:3,933
420	Philippe	2,922	1:3,935
421	Vereecken	2,919	1:3,939
422	Laenen	2,914	1:3,945
423	Bourguignon	2,891	1:3,977
424	de Meester	2,886	1:3,984
425	Delmotte	2,882	1:3,989
426	Loos	2,881	1:3,991
427	Barbier	2,875	1:3,999
428	Collet	2,874	1:4,000
429	Buysse	2,858	1:4,023
430	Van Loon	2,855	1:4,027
431	Michielsen	2,851	1:4,032
432	Lenoir	2,849	1:4,035
433	de Wachter	2,848	1:4,037
434	Remacle	2,845	1:4,041
435	van Lierde	2,833	1:4,058
436	van den Heuvel	2,832	1:4,060
437	Clement	2,830	1:4,062
438	Bruneel	2,826	1:4,068
439	Heremans	2,819	1:4,078
440	Gonzalez	2,815	1:4,084
441	Godart	2,809	1:4,093
442	van der Linden	2,807	1:4,096
443	Hellemans	2,805	1:4,099
444	van Gestel	2,800	1:4,106
445	Mees	2,799	1:4,107
446	Boon	2,792	1:4,118
447	Buyse	2,788	1:4,124
448	Durieux	2,786	1:4,127
448	Rutten	2,786	1:4,127
450	Roosen	2,785	1:4,128
451	de Cuyper	2,784	1:4,130
452	Léonard	2,782	1:4,133
453	van Campenhout	2,780	1:4,135
454	Horemans	2,773	1:4,146
455	de Graeve	2,771	1:4,149
456	Legros	2,769	1:4,152
456	Noël	2,769	1:4,152
456	Willaert	2,769	1:4,152
459	Celis	2,756	1:4,171
460	Steyaert	2,749	1:4,182
461	Bruggeman	2,748	1:4,184
462	Dejonghe	2,747	1:4,185
463	Brouwers	2,741	1:4,194
463	Sabbe	2,741	1:4,194
463	van der Auwera	2,741	1:4,194
466	Franck	2,738	1:4,199
467	Dumoulin	2,736	1:4,202
467	Paquet	2,736	1:4,202
469	Huys	2,732	1:4,208
470	Put	2,731	1:4,210
471	de Schutter	2,729	1:4,213
471	Pierard	2,729	1:4,213
473	Colpaert	2,721	1:4,225
474	Demuynck	2,714	1:4,236
475	Orban	2,688	1:4,277
476	Dekeyser	2,685	1:4,282
477	Liekens	2,674	1:4,299
478	Lammens	2,673	1:4,301
479	Fierens	2,672	1:4,303
480	de Bondt	2,659	1:4,324
481	van Laer	2,658	1:4,325
482	Rodriguez	2,653	1:4,333
483	Dillen	2,642	1:4,351
483	Reynaert	2,642	1:4,351
485	Bertels	2,640	1:4,355
486	Charles	2,632	1:4,368
487	Demeyer	2,631	1:4,370
487	Vereecke	2,631	1:4,370
489	Casier	2,628	1:4,375
489	van Impe	2,628	1:4,375
491	Vranckx	2,627	1:4,376
492	Schoofs	2,624	1:4,381
493	de Mulder	2,621	1:4,386
494	Laureys	2,617	1:4,393
495	Paulus	2,615	1:4,396
495	van Looveren	2,615	1:4,396
497	Feys	2,612	1:4,401
498	Vinck	2,609	1:4,407
499	Depuydt	2,597	1:4,427
500	de Mey	2,591	1:4,437
501	Franssen	2,585	1:4,447
502	Samyn	2,578	1:4,460
503	Theunis	2,577	1:4,461
504	Rosseel	2,574	1:4,466
505	Vermeir	2,566	1:4,480
506	Deleu	2,564	1:4,484
507	Standaert	2,562	1:4,487
508	de Leeuw	2,561	1:4,489
508	Vanhee	2,561	1:4,489
510	Dupuis	2,552	1:4,505
511	Timmerman	2,551	1:4,507
512	Dewilde	2,550	1:4,508
513	Rogiers	2,546	1:4,516
514	Holvoet	2,541	1:4,524
515	Berghmans	2,540	1:4,526
516	Troch	2,522	1:4,559
517	Verbiest	2,520	1:4,562
518	Braem	2,519	1:4,564
519	Moreels	2,518	1:4,566
520	Adams	2,507	1:4,586
521	Vandermeulen	2,506	1:4,588
522	Jadot	2,505	1:4,589
523	Lallemand	2,503	1:4,593
524	de Wulf	2,499	1:4,600
525	Raeymaekers	2,493	1:4,612
526	Rombouts	2,490	1:4,617
527	Beeckman	2,472	1:4,651
527	de Bruycker	2,472	1:4,651
529	Knockaert	2,463	1:4,668
530	Godefroid	2,460	1:4,673
531	Sels	2,452	1:4,689
532	Matthijs	2,441	1:4,710
533	Adriaensen	2,435	1:4,721
534	Devolder	2,425	1:4,741
534	Grégoire	2,425	1:4,741
536	de Rycke	2,421	1:4,749
537	Ramaekers	2,416	1:4,759
538	Fransen	2,410	1:4,770
538	Vermeylen	2,410	1:4,770
540	Walravens	2,402	1:4,786
541	Bossuyt	2,394	1:4,802
541	Joos	2,394	1:4,802
543	Demeyere	2,393	1:4,804
544	Vandersmissen	2,391	1:4,808
545	Deman	2,389	1:4,812
545	Minne	2,389	1:4,812
547	Lucas	2,388	1:4,814
548	Devriendt	2,386	1:4,818
548	Willekens	2,386	1:4,818
550	Delaere	2,385	1:4,820
551	Lahaye	2,376	1:4,839
552	Degroote	2,374	1:4,843
553	Proost	2,372	1:4,847
554	Rombaut	2,370	1:4,851
555	Demaret	2,368	1:4,855
556	Boeckx	2,361	1:4,869
557	van Loock	2,357	1:4,878
557	Vissers	2,357	1:4,878
559	Gevaert	2,349	1:4,894
560	Mouton	2,346	1:4,901
561	Faes	2,344	1:4,905
562	Blondeel	2,343	1:4,907
563	Blommaert	2,334	1:4,926
564	Nelissen	2,333	1:4,928
565	Albert	2,324	1:4,947
565	Druart	2,324	1:4,947
567	Tielemans	2,321	1:4,953
568	Bouckaert	2,319	1:4,958
569	Renson	2,312	1:4,973
569	Vergauwen	2,312	1:4,973
571	Lopez	2,310	1:4,977
572	Meyers	2,308	1:4,981
573	Degryse	2,303	1:4,992
573	Vervaet	2,303	1:4,992
575	Andre	2,297	1:5,005
575	Lemoine	2,297	1:5,005
577	de Mol	2,295	1:5,009
578	Derycke	2,290	1:5,020
578	van Laethem	2,290	1:5,020
580	Machiels	2,285	1:5,031
581	Dardenne	2,284	1:5,034
582	Voet	2,283	1:5,036
583	Mariën	2,272	1:5,060
584	Sanders	2,269	1:5,067
585	Paquay	2,267	1:5,071
586	de Weerdt	2,264	1:5,078
587	de Ryck	2,261	1:5,085
587	Meulemans	2,261	1:5,085
589	Vandeput	2,256	1:5,096
590	Dierick	2,252	1:5,105
591	Henrard	2,245	1:5,121
591	Neyens	2,245	1:5,121
593	Ceuppens	2,244	1:5,123
594	Meersman	2,242	1:5,128
595	Saelens	2,240	1:5,132
596	Joly	2,239	1:5,135
597	van Hoecke	2,238	1:5,137
598	Verfaillie	2,237	1:5,139
599	Couvreur	2,236	1:5,142
600	Vandenbosch	2,235	1:5,144
601	van de Putte	2,223	1:5,172
602	de Roover	2,222	1:5,174
603	Huart	2,219	1:5,181
604	Poelmans	2,216	1:5,188
605	Praet	2,215	1:5,190
606	van Dooren	2,212	1:5,197
606	van Nuffel	2,212	1:5,197
608	Breugelmans	2,209	1:5,204
608	Duchateau	2,209	1:5,204
610	Gustin	2,208	1:5,207
611	Wynants	2,207	1:5,209
612	Vandenabeele	2,205	1:5,214
613	Belmans	2,200	1:5,226
614	Flament	2,197	1:5,233
614	Gilis	2,197	1:5,233
614	Maréchal	2,197	1:5,233
614	van Vaerenbergh	2,197	1:5,233
618	Meyer	2,196	1:5,235
619	van der Veken	2,192	1:5,245
620	Delforge	2,190	1:5,250
621	Dumortier	2,189	1:5,252
622	Snauwaert	2,185	1:5,262
623	de Baere	2,184	1:5,264
624	van Hulle	2,180	1:5,274
624	Vervaeke	2,180	1:5,274
626	Demeulemeester	2,178	1:5,279
627	Cardon	2,168	1:5,303
628	Neven	2,163	1:5,315
629	Verwimp	2,156	1:5,332
630	Beyens	2,153	1:5,340
630	Sanchez	2,153	1:5,340
632	Poncin	2,152	1:5,342
632	Verstappen	2,152	1:5,342
634	D'Haese	2,144	1:5,362
635	Kerkhofs	2,143	1:5,365
635	van der Heyden	2,143	1:5,365
637	de Muynck	2,140	1:5,372
638	van Landeghem	2,137	1:5,380
639	Coene	2,136	1:5,382
640	van Genechten	2,135	1:5,385
641	Schollaert	2,133	1:5,390
642	Bosman	2,130	1:5,397
642	Dieu	2,130	1:5,397
644	Mathy	2,129	1:5,400
645	Beernaert	2,127	1:5,405
646	Demeulenaere	2,120	1:5,423
646	Denayer	2,120	1:5,423
648	Doyen	2,117	1:5,431
648	van Praet	2,117	1:5,431
650	van Cauwenberghe	2,116	1:5,433
651	Neirynck	2,110	1:5,449
652	Deceuninck	2,107	1:5,456
653	da Silva	2,104	1:5,464
653	Richard	2,104	1:5,464
653	Vanbrabant	2,104	1:5,464
653	Wyns	2,104	1:5,464
657	Seynaeve	2,100	1:5,475
658	Minet	2,099	1:5,477
659	Kenis	2,097	1:5,482
660	Wery	2,096	1:5,485
661	Cooreman	2,092	1:5,496
662	Hens	2,088	1:5,506
662	van Raemdonck	2,088	1:5,506
664	Slegers	2,085	1:5,514
665	Baele	2,083	1:5,519
665	Eggermont	2,083	1:5,519
667	van de Vijver	2,078	1:5,533
668	van Poucke	2,077	1:5,535
669	Bernaerts	2,073	1:5,546
670	Delporte	2,065	1:5,567
671	Simoens	2,063	1:5,573
672	Thienpont	2,061	1:5,578
673	Geudens	2,060	1:5,581
673	Scheers	2,060	1:5,581
675	Malfait	2,059	1:5,584
676	Grandjean	2,057	1:5,589
677	Gijbels	2,056	1:5,592
678	Demey	2,055	1:5,594
678	Theunissen	2,055	1:5,594
680	Moonen	2,053	1:5,600
681	Bauduin	2,052	1:5,603
682	Gilbert	2,051	1:5,605
683	Buys	2,049	1:5,611
683	Galle	2,049	1:5,611
685	Vandenbulcke	2,048	1:5,614
686	de Groof	2,046	1:5,619
687	Benoit	2,043	1:5,627
688	Schuermans	2,036	1:5,647
689	Rottiers	2,032	1:5,658
690	van der Elst	2,026	1:5,675
691	Claesen	2,019	1:5,694
692	van Baelen	2,015	1:5,706
693	Perez	2,013	1:5,711
694	Schreurs	2,011	1:5,717
695	Dockx	2,009	1:5,723
696	Bah	2,006	1:5,731
697	van Bogaert	2,001	1:5,745
698	Lardinois	1,996	1:5,760
698	Martinez	1,996	1:5,760
700	Roelants	1,995	1:5,763
701	de Becker	1,992	1:5,771
701	Spruyt	1,992	1:5,771
703	Elsen	1,989	1:5,780
704	Demol	1,985	1:5,792
704	Maris	1,985	1:5,792
706	Gabriels	1,976	1:5,818
707	Billen	1,975	1:5,821
708	Sauvage	1,974	1:5,824
709	Broeckx	1,973	1:5,827
709	Daenen	1,973	1:5,827
711	Staes	1,972	1:5,830
711	van den Abeele	1,972	1:5,830
713	Cornelissen	1,970	1:5,836
714	Collette	1,969	1:5,839
714	Duvivier	1,969	1:5,839
716	Dewit	1,965	1:5,851
717	Foucart	1,960	1:5,866
718	Motte	1,957	1:5,875
719	Bailly	1,951	1:5,893
720	Swerts	1,947	1:5,905
721	Dhont	1,943	1:5,917
722	Coomans	1,941	1:5,923
723	Serneels	1,936	1:5,938
724	Renier	1,935	1:5,941
725	Smeyers	1,934	1:5,944
726	Bergmans	1,931	1:5,954
727	Bal	1,928	1:5,963
727	de Roo	1,928	1:5,963
727	Dechamps	1,928	1:5,963
727	Vilain	1,928	1:5,963
731	Humblet	1,925	1:5,972
731	Verhaert	1,925	1:5,972
733	Adriaenssens	1,924	1:5,975
733	Cuyvers	1,924	1:5,975
733	Geuens	1,924	1:5,975
736	Berckmans	1,921	1:5,985
737	Bovy	1,919	1:5,991
737	Huygens	1,919	1:5,991
739	Storms	1,916	1:6,000
740	Valcke	1,915	1:6,003
741	Hofman	1,914	1:6,007
741	van Opstal	1,914	1:6,007
743	Verbrugghe	1,913	1:6,010
744	de Vriendt	1,911	1:6,016
745	Vleugels	1,910	1:6,019
746	Herremans	1,909	1:6,022
747	Mahy	1,908	1:6,025
748	Vandeweyer	1,907	1:6,029
748	Volders	1,907	1:6,029
750	van Kerckhoven	1,904	1:6,038
751	Jordens	1,902	1:6,045
751	van Mol	1,902	1:6,045
753	Vandenbroeck	1,900	1:6,051
754	Bellemans	1,897	1:6,060
755	Pire	1,896	1:6,064
756	Geurts	1,895	1:6,067
757	Vandevoorde	1,894	1:6,070
758	Colman	1,887	1:6,093
758	Hendrikx	1,887	1:6,093
760	Bervoets	1,885	1:6,099
760	de Corte	1,885	1:6,099
762	Goyvaerts	1,884	1:6,102
763	Thielemans	1,881	1:6,112
764	Grosjean	1,876	1:6,128
765	Wolfs	1,873	1:6,138
766	Debroux	1,872	1:6,141
767	Casteleyn	1,871	1:6,145
767	Englebert	1,871	1:6,145
767	Romain	1,871	1:6,145
770	de Jaeger	1,868	1:6,155
770	Vanacker	1,868	1:6,155
772	Vuylsteke	1,862	1:6,174
772	Wagemans	1,862	1:6,174
774	Swennen	1,860	1:6,181
775	Maenhout	1,857	1:6,191
776	de Wever	1,856	1:6,194
777	Smekens	1,854	1:6,201
778	van Cutsem	1,853	1:6,204
779	de Cooman	1,850	1:6,214
780	Verbraeken	1,847	1:6,224
781	Degraeve	1,845	1:6,231
781	Delcroix	1,845	1:6,231
781	Willem	1,845	1:6,231
784	Haegeman	1,843	1:6,238
785	Vlaeminck	1,842	1:6,241
786	Six	1,841	1:6,245
787	Claerhout	1,838	1:6,255
788	Gabriel	1,837	1:6,258
789	Poels	1,836	1:6,262
790	Naert	1,835	1:6,265
790	Parent	1,835	1:6,265
792	Paul	1,833	1:6,272
793	Alexandre	1,832	1:6,275
794	van Renterghem	1,831	1:6,279
795	van Mechelen	1,829	1:6,286
796	Ghislain	1,828	1:6,289
797	de Buck	1,825	1:6,300
797	de Neef	1,825	1:6,300
799	Bellens	1,824	1:6,303
800	Geldhof	1,820	1:6,317
801	de Geest	1,816	1:6,331
801	Govaert	1,816	1:6,331
803	Delrue	1,815	1:6,334
803	Marlier	1,815	1:6,334
803	Masset	1,815	1:6,334
803	Snoeck	1,815	1:6,334
807	Carton	1,814	1:6,338
808	Vanlerberghe	1,812	1:6,345
809	Courtois	1,808	1:6,359
810	Buyle	1,806	1:6,366
811	Doms	1,803	1:6,376
812	Godfroid	1,802	1:6,380
812	Hansen	1,802	1:6,380
814	Debacker	1,801	1:6,383
815	Salmon	1,800	1:6,387
816	Reniers	1,799	1:6,391
817	Gillain	1,795	1:6,405
817	Laeremans	1,795	1:6,405
819	van Droogenbroeck	1,792	1:6,416
820	Aelbrecht	1,787	1:6,433
821	Truyens	1,786	1:6,437
822	Devriese	1,780	1:6,459
823	de Keyzer	1,779	1:6,462
824	Verstrepen	1,775	1:6,477
825	Polfliet	1,774	1:6,481
826	Moulin	1,770	1:6,495
827	Laloux	1,769	1:6,499
827	Windels	1,769	1:6,499
829	Hofmans	1,768	1:6,503
830	Lienard	1,766	1:6,510
831	Billiet	1,763	1:6,521
831	Pelgrims	1,763	1:6,521
831	Wils	1,763	1:6,521
834	Dubuisson	1,759	1:6,536
834	Duchesne	1,759	1:6,536
836	de Prins	1,758	1:6,540
837	Deville	1,757	1:6,543
837	Ledoux	1,757	1:6,543
837	Neyt	1,757	1:6,543
840	Braeckman	1,756	1:6,547
841	de Man	1,753	1:6,558
842	Adriaens	1,749	1:6,573
843	Kempeneers	1,747	1:6,581
843	Kerremans	1,747	1:6,581
845	Dams	1,746	1:6,585
846	Vleminckx	1,741	1:6,603
847	Lambotte	1,740	1:6,607
847	van der Borght	1,740	1:6,607
849	Vanherck	1,736	1:6,622
850	Vanderhaegen	1,734	1:6,630
851	Heirman	1,732	1:6,638
852	Baudoux	1,729	1:6,649
852	Crombez	1,729	1:6,649
852	Vandenbossche	1,729	1:6,649
855	Maquet	1,724	1:6,669
856	Coulon	1,723	1:6,672
857	Vangeel	1,721	1:6,680
857	Voets	1,721	1:6,680
859	Bielen	1,718	1:6,692
859	de Visscher	1,718	1:6,692
861	Goemaere	1,717	1:6,696
862	Beeckmans	1,716	1:6,700
862	Duquesne	1,716	1:6,700
864	de Geyter	1,715	1:6,704
864	Peetermans	1,715	1:6,704
866	Halleux	1,714	1:6,707
867	van Aken	1,713	1:6,711
868	de Winne	1,712	1:6,715
868	van Kerckhove	1,712	1:6,715
870	Bijnens	1,711	1:6,719
871	Bastiaens	1,708	1:6,731
871	Yilmaz	1,708	1:6,731
873	Glorieux	1,706	1:6,739
874	Sonck	1,705	1:6,743
875	Winters	1,704	1:6,747
876	Soenen	1,702	1:6,755
877	van Geel	1,700	1:6,763
878	Russo	1,697	1:6,775
879	Balcaen	1,696	1:6,779
879	Struyf	1,696	1:6,779
881	Lefèvre	1,694	1:6,787
881	van Malderen	1,694	1:6,787
883	Culot	1,693	1:6,791
883	Dehon	1,693	1:6,791
883	van Cauter	1,693	1:6,791
883	van Nieuwenhove	1,693	1:6,791
887	Diels	1,691	1:6,799
887	Saenen	1,691	1:6,799
889	Vanhecke	1,690	1:6,803
890	Bruyneel	1,688	1:6,811
891	Provoost	1,687	1:6,815
892	Chapelle	1,685	1:6,823
892	Hendriks	1,685	1:6,823
894	van Gucht	1,684	1:6,827
894	Wijnants	1,684	1:6,827
896	Decorte	1,680	1:6,843
897	Vanhoof	1,678	1:6,851
898	Kindt	1,677	1:6,855
898	Noppe	1,677	1:6,855
900	Anciaux	1,674	1:6,868
901	de Zutter	1,672	1:6,876
902	de Potter	1,671	1:6,880
902	Verlinde	1,671	1:6,880
904	Blanckaert	1,670	1:6,884
904	de Maesschalck	1,670	1:6,884
906	Velghe	1,669	1:6,888
907	Landuyt	1,666	1:6,901
908	Becker	1,665	1:6,905
908	Verbeek	1,665	1:6,905
910	Wille	1,664	1:6,909
911	Keppens	1,661	1:6,922
912	de Munck	1,660	1:6,926
912	Soete	1,660	1:6,926
914	Lens	1,658	1:6,934
914	van Reeth	1,658	1:6,934
916	Bury	1,657	1:6,938
916	de Schryver	1,657	1:6,938
918	Verplancke	1,656	1:6,942
919	Closset	1,654	1:6,951
919	Nagels	1,654	1:6,951
919	Raskin	1,654	1:6,951
919	van Bever	1,654	1:6,951
919	van de Wiele	1,654	1:6,951
919	van den Steen	1,654	1:6,951
919	Vergote	1,654	1:6,951
926	Geeroms	1,653	1:6,955
926	Konings	1,653	1:6,955
928	Steegmans	1,652	1:6,959
929	Matagne	1,651	1:6,963
930	Collart	1,648	1:6,976
931	Lambot	1,646	1:6,985
931	Maesen	1,646	1:6,985
933	Rogge	1,643	1:6,997
934	Gomez	1,641	1:7,006
935	Vanhees	1,639	1:7,014
935	Vyncke	1,639	1:7,014
937	Frans	1,638	1:7,019
938	Leduc	1,637	1:7,023
938	van Dam	1,637	1:7,023
940	Geukens	1,635	1:7,032
941	Fabry	1,628	1:7,062
942	Deschamps	1,627	1:7,066
943	Danneels	1,626	1:7,071
944	Coucke	1,625	1:7,075
944	Devillers	1,625	1:7,075
944	Hoornaert	1,625	1:7,075
944	Meuleman	1,625	1:7,075
944	Ruelle	1,625	1:7,075
949	Audenaert	1,624	1:7,079
950	Goeminne	1,623	1:7,084
951	Gevers	1,619	1:7,101
952	Seys	1,618	1:7,105
952	van Riet	1,618	1:7,105
954	Goffinet	1,615	1:7,119
955	Caudron	1,614	1:7,123
956	Broos	1,613	1:7,127
957	Liegeois	1,612	1:7,132
958	Werbrouck	1,611	1:7,136
959	Jadoul	1,606	1:7,159
960	van Bael	1,605	1:7,163
961	Tytgat	1,604	1:7,167
962	Delattre	1,601	1:7,181
963	de Haes	1,600	1:7,185
963	Schelfhout	1,600	1:7,185
965	Briers	1,597	1:7,199
966	van Parys	1,596	1:7,203
967	de Vreese	1,594	1:7,212
968	Vanhaverbeke	1,590	1:7,231
968	Vervloet	1,590	1:7,231
970	Meganck	1,586	1:7,249
970	Moortgat	1,586	1:7,249
970	van Cauwenbergh	1,586	1:7,249
973	Bex	1,582	1:7,267
973	Lefever	1,582	1:7,267
973	Vekemans	1,582	1:7,267
976	Bouchez	1,573	1:7,309
977	Weber	1,572	1:7,313
978	van Aerschot	1,571	1:7,318
979	Blockx	1,569	1:7,327
979	Durant	1,569	1:7,327
981	Knapen	1,568	1:7,332
982	Knaepen	1,564	1:7,351
982	Persoons	1,564	1:7,351
984	Vandormael	1,563	1:7,355
985	Bouchat	1,560	1:7,370
986	Beelen	1,559	1:7,374
986	Petitjean	1,559	1:7,374
988	Embrechts	1,558	1:7,379
988	van Eynde	1,558	1:7,379
990	Simonis	1,557	1:7,384
991	Deroo	1,555	1:7,393
992	vander Elst	1,554	1:7,398
993	Philippart	1,553	1:7,403
994	de Troyer	1,550	1:7,417
995	Schoeters	1,549	1:7,422
995	Vertongen	1,549	1:7,422
997	de Vlieger	1,548	1:7,427
997	van der Straeten	1,548	1:7,427
999	Kesteloot	1,545	1:7,441
1000	Depauw	1,544	1:7,446
1000	Muylaert	1,544	1:7,446